﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Dasic.Library
{
    public static class Common
    {
        /// <summary>
        ///     弹出消息提示框
        /// </summary>
        /// <param name="isok">返回正常信息或者警告信息</param>
        /// <param name="msg">返回的信息的内容</param>
        public static void Msg(bool isok, string msg)
        {
            MessageBox.Show(msg, "提示", MessageBoxButton.OK, isok ? MessageBoxImage.Information : MessageBoxImage.Error);
        }


        /// <summary>
        ///     接装连接字符串
        /// </summary>
        /// <param name="ip">主机的IP</param>
        /// <param name="port">主机的端口</param>
        /// <param name="user">用户名</param>
        /// <param name="pass">密码</param>
        /// <param name="table">需要连接的库</param>
        /// <returns></returns>
        public static string ConnectionStr(string ip, int port, string user, string pass, string table)
        {
            return $"Server={ip};Port={port};Database={table};Uid={user};Pwd={pass}";
        }

        /// <summary>
        ///     创建右键菜单子项
        /// </summary>
        /// <param name="name">菜单名</param>
        /// <param name="img">菜单图标</param>
        /// <returns></returns>
        public static MenuItem ContextMenuParentCreatItem(string name, string img)
        {
            return new MenuItem
            {
                Header = name,
                Icon = new Image
                {
                    Source = new BitmapImage(new Uri($"../Images/{img}", UriKind.RelativeOrAbsolute))
                }
            };
        }

        /// <summary>
        ///     创建右键菜单子项
        /// </summary>
        /// <param name="notDiff">左右两边不一样</param>
        /// <param name="pos">位置</param>
        /// <param name="headleft">左边菜单名</param>
        /// <param name="headright">右边菜单名</param>
        /// <param name="imgleft">左边菜单图标</param>
        /// <param name="imgright">右边菜单图标</param>
        /// <returns></returns>
        public static MenuItem ContextMenuParentCreatItem(bool notDiff, int pos, string headleft, string headright,
            string imgleft, string imgright)
        {
            return new MenuItem
            {
                Header = pos == 1 ? headright : headleft,
                IsEnabled = !notDiff,
                Icon =
                    new Image
                    {
                        Source =
                            new BitmapImage(new Uri(pos == 1 ? $"../Images/{imgright}" : $"../Images/{imgleft}",
                                UriKind.RelativeOrAbsolute))
                    }
            };
        }
    }
}